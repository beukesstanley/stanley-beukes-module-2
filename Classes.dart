void main() {
  var WinningApp = new Apps();

  WinningApp.name = "EasyEquities";
  WinningApp.sector = "Finance";
  WinningApp.developer = "Charles Savage";
  WinningApp.year = "2020";

  WinningApp.printAppInformation();
  var Appname = "EasyEquities";
  var Capitals = Appname.toUpperCase();
  print("\n b) The app name in all capital letters: $Capitals");
}

class Apps {
  String? name;
  String? sector;
  String? developer;
  String? year;

  void printAppInformation() {
    print("Winning app name is $name.");
    print("Winning app sector is $sector.");
    print("Winning app developer is $developer.");
    print("Winning year  is $year.");
  }
}
